package mapReduceAuto;

import java.io.IOException;
import java.net.URI;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GeneratedMapper extends Mapper<LongWritable,Text,Text,Text>{

	String fieldSeparator;
	private Filter[] filters;
	private String filename;
	private int[] keyNumbers;
	
	@Override
	protected void setup(Mapper<LongWritable, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {
		filename = "";
		URI[] localFiles = DistributedCache.getCacheFiles(context.getConfiguration());
		for (URI eachPath : localFiles) {
	        filename = eachPath.toString().trim();
	        if(filename.contains(".xml")) {
	        	break;
	        }
	        
		}
		readXML();
	}

	private void readXML() throws IOException {
		Document mapReduceDoc;
		try {
			mapReduceDoc = MapReduceGenerator.parseXML(filename);
			fieldSeparator = mapReduceDoc.getElementsByTagName("input-format").item(0)
					.getAttributes().getNamedItem("column-separator").getTextContent().trim();
			NodeList filterNodes = mapReduceDoc.getElementsByTagName("filter");
			filters = new Filter[filterNodes.getLength()];
			for(int i=0;i<filterNodes.getLength();i++) {
				NamedNodeMap attr = filterNodes.item(i).getAttributes();
				filters[i] = new Filter(Integer.parseInt(attr.getNamedItem("field-number").getTextContent()), 
						attr.getNamedItem("comparator").getTextContent(), 
						attr.getNamedItem("value").getTextContent());
			}
			
			NodeList keyNodes = mapReduceDoc.getElementsByTagName("field");
			 
			keyNumbers = new int[keyNodes.getLength()];
			for(int i=0;i<keyNodes.getLength();i++) {
				keyNumbers[i] = Integer.parseInt(keyNodes.item(i).getAttributes().getNamedItem("number").getTextContent());
				//System.out.println("key fields:"+keyNumbers[i]);
			}
			
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void map(LongWritable key, Text value, Mapper<LongWritable, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {
		String line = value.toString();
		String[] fields = line.split(fieldSeparator);
		boolean filterResult = false;
		for(int i=0;i<filters.length;i++) {
			String field = fields[filters[i].field];
			String comparator = filters[i].comparator;
			String filterValue = filters[i].value;
			if(i == 0)
				filterResult = compare(field,comparator,filterValue);
			else
				filterResult &= compare(field,comparator,filterValue);
		}
		if(filterResult) {
			String groupKey="";
			for(int i=0;i<keyNumbers.length;i++) {
				if(i == (keyNumbers.length-1))
					groupKey += fields[keyNumbers[i]];
				else
					groupKey += fields[keyNumbers[i]]+"		";
			}
			
			context.write(new Text(fields[28]+"	"+fields[1].substring(0, 4)), new Text(fields[5]));
		}
		//else
			//System.out.println("filtered out:"+line);
	}

	private boolean compare(String field, String comparator, String filterValue) {
		//System.out.println("compare: "+field +" "+comparator+ " "+filterValue);
		if(comparator.equals("!=")) 
			return (!field.equalsIgnoreCase(filterValue));
		else if(comparator.equals("=="))
			return (field.equalsIgnoreCase(filterValue));
		else {
			//numerical comparison
		}
		return false;
	}
}