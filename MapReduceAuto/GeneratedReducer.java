package mapReduceAuto;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class GeneratedReducer extends Reducer<Text,Text,Text,Text> {

	@Override
	protected void reduce(Text key, Iterable<Text> values, Reducer<Text, Text, Text, Text>.Context context)
			throws IOException, InterruptedException {
		double sum=0.0;
		int count =0;
		for(Text value: values) {
			count++;
			sum += Double.parseDouble(value.toString());
		}
		context.write(key, new Text(Double.toString(sum/count)));
	}

}
