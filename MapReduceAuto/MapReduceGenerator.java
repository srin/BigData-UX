package mapReduceAuto;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder; 
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class MapReduceGenerator {
	private MapReduceJob job;
	
	public static Document parseXML(String filename) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.parse(new File(filename));
		return doc;
	}
	public MapReduceGenerator(String filename) throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, InterruptedException {
		
		job = new MapReduceJob();
		job.readMapReduceJob(filename);
		job.submitMRJob();
	}
	
	/*public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, ClassNotFoundException, InterruptedException {
		MapReduceGenerator mrGenerator = new MapReduceGenerator(args[0]);
		
	}*/
}