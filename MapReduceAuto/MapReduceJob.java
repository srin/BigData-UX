package mapReduceAuto;

import java.io.IOException;
import java.net.URI;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.OutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class MapReduceJob {
	public static final String FIELD_SEPARATOR = "fieldSeparator";
	private static final String outputFormatPackage = "org.apache.hadoop.mapreduce.lib.output.";
	private static final String inputFormatPackage = "org.apache.hadoop.mapreduce.lib.input.";
	private String fileName;
	String inputPath;
	String outputPath;
	String inputFormatClass;
	String outputFormatClass;
	String fieldSeparator;
	int numberOfReducers;
	Filter[] filters;
	
	public void setInputPath(String inputPath) {
		this.inputPath = inputPath;
		System.out.println("set input path to "+inputPath);
	}
	public void setOutputPath(String outputPath) {
		this.outputPath = outputPath;
		System.out.println("set output to "+outputPath);
	}
	public void setInputFormatClass(String inputFormatClass) {
		this.inputFormatClass = inputFormatClass;
		System.out.println("set input format class to "+inputFormatClass);
	}
	public void setNumberOfReducers(int numberOfReducers) {
		this.numberOfReducers = numberOfReducers;
		System.out.println("set number of reducers to "+numberOfReducers);
	}
	public void setOutputFormatClass(String outputFormatClass) {
		this.outputFormatClass = outputFormatClass;
		System.out.println("set output format class to "+outputFormatClass);
	}
	public void setFieldSeparator(String fieldSeparator) {
		this.fieldSeparator = fieldSeparator;
		System.out.println("set column separator to "+fieldSeparator);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void submitMRJob() throws ClassNotFoundException, IOException, InterruptedException {
		Configuration conf = new Configuration();
		conf.set("fs.hdfs.impl", 
		        org.apache.hadoop.hdfs.DistributedFileSystem.class.getName()
		    );
		conf.set("fs.file.impl",
		        org.apache.hadoop.fs.LocalFileSystem.class.getName()
		    );
		FileSystem  hdfs = FileSystem.get(URI.create(outputPath), conf);
		hdfs.delete(new Path(outputPath), true);
	    
	    	//conf.set(FIELD_SEPARATOR, fieldSeparator);
		DistributedCache.addCacheFile(new Path(fileName).toUri(), conf);    	
	    	Job job = new Job(conf, "GeneraredMR");
			job.setJarByClass(MapReduceGenerator.class);
			
	
			
		    FileInputFormat.addInputPath(job, new Path(inputPath));
		    job.setInputFormatClass((Class<? extends InputFormat>) 
		    		Class.forName(inputFormatPackage+inputFormatClass));
		    		    
		    job.setMapperClass(GeneratedMapper.class);
		    
		    job.setMapOutputKeyClass(Text.class);
		    job.setMapOutputValueClass(Text.class);
		    
		    job.setNumReduceTasks(numberOfReducers);
		    job.setReducerClass(GeneratedReducer.class);
		    
		    job.setOutputKeyClass(Text.class);
		    job.setOutputValueClass(Text.class);
		    
		    Path outPath = new Path(outputPath);
		    FileOutputFormat.setOutputPath(job, outPath);
		    job.setOutputFormatClass((Class<? extends OutputFormat>) 
		    		Class.forName(outputFormatPackage+outputFormatClass));
		    
		    boolean succeeded = job.waitForCompletion(true);
		    if (!succeeded) {
		      throw new IllegalStateException("findMaxCrimeCount Job failed!");
		    }
	}
	public void readMapReduceJob(String filename) throws ParserConfigurationException, SAXException, IOException {
		fileName = filename;
		Document mapReduceDoc = MapReduceGenerator.parseXML(filename);
		System.out.println("Root element: " + mapReduceDoc.getDocumentElement().getNodeName());
		setInputPath(mapReduceDoc.getElementsByTagName("input-path").item(0).getTextContent().trim());
		setOutputPath(mapReduceDoc.getElementsByTagName("output-path").item(0).getTextContent());
		setInputFormatClass(mapReduceDoc.getElementsByTagName("input-format").item(0).
				getAttributes().getNamedItem("class").getTextContent());
		setNumberOfReducers(Integer.parseInt(mapReduceDoc.getElementsByTagName("numberOfReducers").
				item(0).getTextContent()));
		setOutputFormatClass(mapReduceDoc.getElementsByTagName("output-format").item(0).
				getAttributes().getNamedItem("class").getTextContent());
	}
}
