package com.sample.CoreApp;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

@ConfigurationProperties
//@PropertySource("file:application.properties")
@Component
public class GlobalProperties {

		@Value("${inputFileLocation}")
		private String inputFileLocation;
		
		@Bean
		public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
			return new PropertySourcesPlaceholderConfigurer();
		}
		
		public String getInputFileLocation() {
			return inputFileLocation;
		}
}
