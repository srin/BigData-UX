package com.sample.CoreApp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HeaderFieldData {
	@JsonProperty("name")
	public String Name;
	@JsonProperty("dataType")
	public String DataType;
	@JsonProperty("number")
	public String Number;
}
