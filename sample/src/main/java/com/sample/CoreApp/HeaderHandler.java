package com.sample.CoreApp;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.XML;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class HeaderHandler {

	private MultipartFile _file;
	public static final String FOLDERLOCATION = "stuff";
	
	public HeaderHandler(MultipartFile uploadFile)
	{
		_file = uploadFile;
	}
	
	public ArrayList<HeaderFieldData> ProcessFile() throws ParserConfigurationException, SAXException, IOException
	{
		ArrayList<HeaderFieldData> fieldData = new ArrayList<HeaderFieldData>();
		
		byte[] byteArray = _file.getBytes();
		InputStream is = new ByteArrayInputStream(byteArray);  
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder builder = factory.newDocumentBuilder();  
		Document xml = builder.parse(is);
		
		NodeList nodes = xml.getDocumentElement().getElementsByTagName("field");
		for(int i = 0; i < nodes.getLength(); ++i)
		{
			HeaderFieldData data = new HeaderFieldData();
			Element elem = (Element)nodes.item(i);
			data.Number = elem.getAttribute("number");
			data.Name = elem.getElementsByTagName("name").item(0).getTextContent();
			data.DataType = elem.getElementsByTagName("dataType").item(0).getTextContent();
			fieldData.add(data);
		}
		
		SaveUploadedFile(_file);
		
		return fieldData;
	}
	
	public String ValidateFile()
	{
		if(_file.isEmpty()) {
			return "Please select a file!";
		}
		if(!_file.getContentType().equalsIgnoreCase("text/xml")) {
			return "Only XML files are accepted";
		}
		return null;
	}
	
	private void SaveUploadedFile(MultipartFile file) throws IOException{
		
		//Path path = Paths.get(new GlobalProperties().getInputFileLocation() + file.getOriginalFilename());
		File directory = new File(FOLDERLOCATION);
		directory.mkdirs();
		String fileName = FOLDERLOCATION + File.separator + file.getOriginalFilename();
		Path path = Paths.get(fileName);
		byte[] byteArray = _file.getBytes();
		Files.write(path,  byteArray);
	}
}