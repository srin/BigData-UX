package com.sample.CoreApp;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class InputHandler {

	private MultipartFile _file;
	public static final String FOLDERLOCATION = "stuff";
	
	public InputHandler(MultipartFile uploadFile)
	{
		_file = uploadFile;
	}
	
	public String ValidateFile()
	{
		if(_file.isEmpty()) {
			return "Please select a file!";
		}
		String fileType = _file.getContentType();
		if(!fileType.equalsIgnoreCase("text/xml") && !fileType.equalsIgnoreCase("text/plain")) {
			return "Only XML or text files are accepted";
		}
		return null;
	}
	
	public void ProcessFile(ArrayList<FlowComponent> flows, ArrayList<AggregateComponent> aggregates) throws IOException
	{
		//Save the file to local
		SaveUploadedFile(_file);
		BuildFlowFile(_file.getOriginalFilename(), flows, aggregates);
	}
	
	private void SaveUploadedFile(MultipartFile file) throws IOException{
		
		//Path path = Paths.get(new GlobalProperties().getInputFileLocation() + file.getOriginalFilename());
		File directory = new File(FOLDERLOCATION);
		directory.mkdirs();
		String fileName = FOLDERLOCATION + File.separator + file.getOriginalFilename();
		Path path = Paths.get(fileName);
		byte[] byteArray = _file.getBytes();
		Files.write(path,  byteArray);
	}
	
	private void BuildFlowFile(String inputFileName, ArrayList<FlowComponent> flows, ArrayList<AggregateComponent> aggregates)
	{
		try {

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			
			Document doc = docBuilder.newDocument();
			Element rootElement = doc.createElement("mapReduceJob");
			doc.appendChild(rootElement);
			
			Element inputPath = doc.createElement("input-path");
			inputPath.appendChild(doc.createTextNode("hdfs://localhost:9000/input/" + inputFileName));
			rootElement.appendChild(inputPath);

			Element inputFormat = doc.createElement("input-format");
			Attr attr = doc.createAttribute("class");
			attr.setValue("TextInputFormat");
			inputFormat.setAttributeNode(attr);
			Attr recordSeperator = doc.createAttribute("record-seperator");
			recordSeperator.setValue("\\n");
			inputFormat.setAttributeNode(recordSeperator);
			Attr columnSeperator = doc.createAttribute("column-separator");
			columnSeperator.setValue("\\\\s+");
			inputFormat.setAttributeNode(columnSeperator);
			rootElement.appendChild(inputFormat);
			
			Element inputHeader = doc.createElement("input-header");
			inputHeader.appendChild(doc.createTextNode("header.xml")); //TODO Get this from UI
			rootElement.appendChild(inputHeader);
			
			Element outputPath = doc.createElement("output-path");
			outputPath.appendChild(doc.createTextNode("hdfs://localhost:9000/output")); //TODO Get this from UI
			rootElement.appendChild(outputPath);
			
			Element outputFormat = doc.createElement("output-format");
			Attr outputFormatClass = doc.createAttribute("class");
			outputFormatClass.setValue("TextOutputFormat");
			outputFormat.setAttributeNode(outputFormatClass);
			rootElement.appendChild(outputFormat);
			
			Element numberOfReducers = doc.createElement("numberOfReducers");
			numberOfReducers.appendChild(doc.createTextNode("1"));
			rootElement.appendChild(numberOfReducers);
			
			Element filters = doc.createElement("filters");
			for(FlowComponent flow : flows)
			{
				Element flowEl = doc.createElement("filter");
				Attr fieldNumber = doc.createAttribute("field-number");
				fieldNumber.setValue(flow.Number);
				flowEl.setAttributeNode(fieldNumber);
				Attr comparator = doc.createAttribute("comparator");
				comparator.setValue(flow.Comparator);
				flowEl.setAttributeNode(comparator);
				Attr valueEl = doc.createAttribute("value");
				valueEl.setValue(flow.Value);
				flowEl.setAttributeNode(valueEl);
				filters.appendChild(flowEl);
			}
			
			rootElement.appendChild(filters);
			
			Element aggregateElement = doc.createElement("aggregates");
			Element aggregateSubElement = doc.createElement("aggregate");
			aggregateElement.appendChild(aggregateSubElement);
			for(AggregateComponent aggregate : aggregates)
			{
				Element aggFunction = doc.createElement("aggregate-function");
				Attr fieldNumber = doc.createAttribute("field");
				fieldNumber.setValue(aggregate.Number);
				aggFunction.setAttributeNode(fieldNumber);
				aggregateSubElement.appendChild(aggFunction);
				Attr comparator = doc.createAttribute("name");
				comparator.setValue(aggregate.Comparator);
				aggFunction.setAttributeNode(comparator);
			}
			
			rootElement.appendChild(aggregateElement);
			
			
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			File file = new File(FOLDERLOCATION + File.separator + "flow.xml");
			StreamResult result = new StreamResult(file);

			
			// Output to console for testing
			// StreamResult result = new StreamResult(System.out);

			transformer.transform(source, result);
			
		}
		catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		  } catch (TransformerException tfe) {
			tfe.printStackTrace();
		  }
	}
}
