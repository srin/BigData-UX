package com.sample;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sample.CoreApp.AggregateComponent;
import com.sample.CoreApp.FlowComponent;
import com.sample.CoreApp.HeaderFieldData;
import com.sample.CoreApp.HeaderHandler;
import com.sample.CoreApp.InputHandler;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@org.springframework.web.bind.annotation.RestController
public class RestController {
 
	@RequestMapping("/api/hello")
	public String greet() {
		return "Hello from the other side!!!";
		}
	
	
	@CrossOrigin()
	@PostMapping("/api/input-upload")
	public ResponseEntity<?> uploadFile(
			@RequestParam("file") MultipartFile uploadFile)
	{
		
		HeaderHandler handler = new HeaderHandler(uploadFile);
		String error = handler.ValidateFile();
		if(error != null)
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		
		ArrayList<HeaderFieldData> data;
		try {
			data = handler.ProcessFile();
			
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.OK);
	}
	
	@CrossOrigin()
	@PostMapping("/api/flow-upload")
	public ResponseEntity<?> uploadFlow(@RequestParam("file") MultipartFile uploadFile, @RequestParam("filters") String filtersStr, @RequestParam("aggregators") String aggregatorsStr)
	{
		//return new ResponseEntity<>("Success", HttpStatus.OK);
		InputHandler handler = new InputHandler(uploadFile);
		String error = handler.ValidateFile();
		if(error != null)
			return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
		
		try {
		ObjectMapper mapper = new ObjectMapper();
		ArrayList<FlowComponent> filters = mapper.readValue(filtersStr, new TypeReference<ArrayList<FlowComponent>>() {});
		
		ArrayList<AggregateComponent> aggregators = mapper.readValue(aggregatorsStr, new TypeReference<ArrayList<AggregateComponent>>() {});
		
		handler.ProcessFile(filters, aggregators);
		} catch(Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>("Success", HttpStatus.OK);
	}
	
}


