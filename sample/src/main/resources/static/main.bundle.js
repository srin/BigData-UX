webpackJsonp(["main"],{

/***/ "../../../../../src/$$_gendir lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_gendir lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "\n\n<h1>Please upload the Header file</h1>\n\n<input id=\"photo\" type=\"file\" />\n<!-- button to trigger the file upload when submitted -->\n<button type=\"button\" class=\"btn btn-success btn-s\" (click)=\"upload()\">\nUpload\n</button>\n\n<app-flow></app-flow>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__header_field_data_service__ = __webpack_require__("../../../../../src/app/header-field-data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//import component, ElementRef, input and the oninit method from angular core

//import the native angular http and respone libraries

//import the do function to be used with the http library.

//import the map function to be used with the http library


var AppComponent = (function () {
    function AppComponent(http, el, headerFieldDataService) {
        this.http = http;
        this.el = el;
        this.headerFieldDataService = headerFieldDataService;
        this.title = 'BigData Sample App';
    }
    AppComponent.prototype.upload = function () {
        //locate the file element meant for the file upload.
        var inputEl = this.el.nativeElement.querySelector('#photo');
        //get the total amount of files attached to the file input.
        var fileCount = inputEl.files.length;
        if (fileCount > 0) {
            this.headerFieldDataService.uploadFile(inputEl.files.item(0));
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__("../../../../../src/app/app.component.html"),
        styles: [__webpack_require__("../../../../../src/app/app.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__header_field_data_service__["a" /* HeaderFieldDataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__header_field_data_service__["a" /* HeaderFieldDataService */]) === "function" && _c || Object])
], AppComponent);

var _a, _b, _c;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/@angular/platform-browser.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__("../../../forms/@angular/forms.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__header_field_data_service__ = __webpack_require__("../../../../../src/app/header-field-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__flow_input_data_service__ = __webpack_require__("../../../../../src/app/flow/input-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__flow_flow_component__ = __webpack_require__("../../../../../src/app/flow/flow.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["L" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_7__flow_flow_component__["a" /* FlowComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_5__header_field_data_service__["a" /* HeaderFieldDataService */], __WEBPACK_IMPORTED_MODULE_6__flow_input_data_service__["a" /* InputDataService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ "../../../../../src/app/flow/flow.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/flow/flow.component.html":
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"headerFieldData\">\r\n\r\n<h1>Please make your selections</h1>\r\n\r\n<h2> Filters</h2>\r\n\r\n<ul>\r\n\r\n\t<li *ngFor=\"let field of filters; let i = index\">\r\n\t\t<select [(ngModel)]=\"field.number\">\r\n\t\t\t<option value=\"\">Choose one</option>\r\n\t\t\t<option *ngFor=\"let fieldData of headerFieldData\" value = {{fieldData.number}}>\r\n\t\t\t{{fieldData.name}} - {{fieldData.number}} \r\n\t\t\t</option>\r\n\t\t</select>\r\n\t\t<select [(ngModel)]=\"field.comparator\">\r\n\t\t\t<option value=\"\">Choose one</option>\r\n\t\t\t<option value=\"=\">=</option>\r\n\t\t\t<option value=\"!=\">!=</option>\r\n\t\t\t<option value=\">\">></option>\r\n\t\t\t<option value=\"<\"><</option>\r\n\t\t</select>\r\n\t\t<input type=\"text\" [(ngModel)]=\"field.value\" />\r\n\t\t\r\n\t\t<button (click)=\"add($event)\">+</button>\r\n\t\t<button (click)=\"remove(i)\">-</button>\r\n\t</li>\r\n</ul>\r\n\r\n<h2>Aggregators</h2>\r\n<ul>\r\n\t\r\n\t<li *ngFor=\"let field of aggregators; let i = index\">\r\n\t\t<select [(ngModel)]=\"field.number\">\r\n\t\t\t<option value=\"\">Choose one</option>\r\n\t\t\t<option *ngFor=\"let fieldData of headerFieldData\" value = {{fieldData.number}}>\r\n\t\t\t{{fieldData.name}} - {{fieldData.number}} \r\n\t\t\t</option>\r\n\t\t</select>\r\n\t\t<select [(ngModel)]=\"field.comparator\">\r\n\t\t\t<option value=\"\">Choose one</option>\r\n\t\t\t<option value=\"average\">Average</option>\r\n\t\t\t<option value=\"sum\">Sum</option>\r\n\t\t\t<option value=\"total\">Total</option>\r\n\t\t\t<option value=\"min\">Min</option>\r\n\t\t\t<option value=\"max\">Max</option>\r\n\t\t</select>\r\n\t\t\r\n\t\t<button (click)=\"addAgg($event)\">+</button>\r\n\t\t<button (click)=\"removeAgg(i)\">-</button>\r\n\t</li>\r\n</ul>\r\n\r\n <br/>\r\n \r\n <h2> Select input file to upload</h2>\r\n <input id=\"inputFile\" type=\"file\" />\r\n \r\n <button type=\"button\" class=\"btn btn-success btn-s\" (click)=\"submitFlow()\">Run Analysis</button>\r\n \r\n </div>"

/***/ }),

/***/ "../../../../../src/app/flow/flow.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FlowComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__header_field_data_service__ = __webpack_require__("../../../../../src/app/header-field-data.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__input_data_service__ = __webpack_require__("../../../../../src/app/flow/input-data.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var URL = 'http://localhost:8080/api/flow-upload';
var FlowComponent = (function () {
    function FlowComponent(headerFieldDataService, http, el, inputDataService) {
        var _this = this;
        this.headerFieldDataService = headerFieldDataService;
        this.http = http;
        this.el = el;
        this.inputDataService = inputDataService;
        this.subscription = headerFieldDataService.headerFieldData$.subscribe(function (data) {
            _this.headerFieldData = data;
        });
        this.filters = [{}];
        this.aggregators = [{}];
    }
    FlowComponent.prototype.ngOnInit = function () {
    };
    FlowComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    FlowComponent.prototype.updateFlowDetails = function (data) {
        this.headerFieldData = data;
    };
    FlowComponent.prototype.add = function () {
        this.filters.push({});
    };
    FlowComponent.prototype.remove = function (index) {
        //console.log(event);
        this.filters.splice(index, 1);
    };
    FlowComponent.prototype.addAgg = function () {
        this.aggregators.push({});
    };
    FlowComponent.prototype.removeAgg = function (index) {
        //console.log(event);
        this.aggregators.splice(index, 1);
    };
    FlowComponent.prototype.submitFlow = function () {
        //console.log(this.filters);
        var inputEl = this.el.nativeElement.querySelector('#inputFile');
        var fileCount = inputEl.files.length;
        if (fileCount > 0) {
            this.inputDataService.uploadFile(inputEl.files.item(0), this.filters, this.aggregators);
        }
    };
    return FlowComponent;
}());
FlowComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["n" /* Component */])({
        selector: 'app-flow',
        template: __webpack_require__("../../../../../src/app/flow/flow.component.html"),
        styles: [__webpack_require__("../../../../../src/app/flow/flow.component.css")]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__header_field_data_service__["a" /* HeaderFieldDataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__header_field_data_service__["a" /* HeaderFieldDataService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ElementRef */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__input_data_service__["a" /* InputDataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__input_data_service__["a" /* InputDataService */]) === "function" && _d || Object])
], FlowComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=flow.component.js.map

/***/ }),

/***/ "../../../../../src/app/flow/input-data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InputDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import the do function to be used with the http library.

//import the map function to be used with the http library

var URL = '/api/flow-upload';
var InputDataService = (function () {
    function InputDataService(http) {
        this.http = http;
    }
    InputDataService.prototype.uploadFile = function (file, filters, aggregators) {
        //create a new fromdata instance
        var formData = new FormData();
        formData.append('file', file);
        formData.append('filters', JSON.stringify(filters));
        formData.append('aggregators', JSON.stringify(aggregators));
        //call the angular http method
        this.http
            .post(URL, formData).subscribe(
        //map the success function and alert the response
        function (success) {
            console.log(success);
        }, function (error) { return alert(error); });
    };
    return InputDataService;
}());
InputDataService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object])
], InputDataService);

var _a;
//# sourceMappingURL=input-data.service.js.map

/***/ }),

/***/ "../../../../../src/app/header-field-data.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeaderFieldDataService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/@angular/http.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__ = __webpack_require__("../../../../rxjs/Subject.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import the do function to be used with the http library.

//import the map function to be used with the http library


var URL = '/api/input-upload';
var HeaderFieldDataService = (function () {
    function HeaderFieldDataService(http) {
        this.http = http;
        this.headerFieldData = new __WEBPACK_IMPORTED_MODULE_4_rxjs_Subject__["Subject"]();
        this.headerFieldData$ = this.headerFieldData.asObservable();
    }
    HeaderFieldDataService.prototype.uploadFile = function (file) {
        var _this = this;
        //create a new fromdata instance
        var formData = new FormData();
        //check if the filecount is greater than zero, to be sure a file was selected.
        //append the key name 'photo' with the first file in the element
        formData.append('file', file);
        //call the angular http method
        this.http
            .post(URL, formData).map(function (res) { return res.json(); }).subscribe(
        //map the success function and alert the response
        function (success) {
            //alert(success._body);
            _this.headerFieldData.next(success);
        }, function (error) { return alert(error); });
    };
    return HeaderFieldDataService;
}());
HeaderFieldDataService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["B" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]) === "function" && _a || Object])
], HeaderFieldDataService);

var _a;
//# sourceMappingURL=header-field-data.service.js.map

/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/@angular/core.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/@angular/platform-browser-dynamic.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_20" /* enableProdMode */])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map