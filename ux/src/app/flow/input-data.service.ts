import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
//import the do function to be used with the http library.
import "rxjs/add/operator/do";
//import the map function to be used with the http library
import "rxjs/add/operator/map";
import { Subject } from "rxjs/Subject";


const URL = '/api/flow-upload';

@Injectable()
export class InputDataService {
  
  constructor(private http: Http) {
		
    }
    
  uploadFile(file:any, filters, aggregators){
  	//create a new fromdata instance
        let formData = new FormData();
                formData.append('file', file);
                formData.append('filters', JSON.stringify(filters));
                formData.append('aggregators', JSON.stringify(aggregators));
            //call the angular http method
            this.http
        //post the form data to the url defined above and map the response. Then subscribe //to initiate the post. if you don't subscribe, angular wont post.
                .post(URL, formData).subscribe(
                //map the success function and alert the response
                 (success) => {
                         console.log(success);
                },
                (error) => alert(error));
  }
}
