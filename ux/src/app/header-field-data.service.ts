import { Injectable } from '@angular/core';
import { HeaderFieldData } from './header-field-data';
import { Http, Response } from '@angular/http';
//import the do function to be used with the http library.
import "rxjs/add/operator/do";
//import the map function to be used with the http library
import "rxjs/add/operator/map";
import { Subject } from "rxjs/Subject";


const URL = '/api/input-upload';

@Injectable()
export class HeaderFieldDataService {
  public headerFieldData = new Subject();
  
  headerFieldData$ = this.headerFieldData.asObservable();
  
  constructor(private http: Http) {
		
    }
    
  uploadFile(file:any){
  	//create a new fromdata instance
        let formData = new FormData();
    //check if the filecount is greater than zero, to be sure a file was selected.
            //append the key name 'photo' with the first file in the element
                formData.append('file', file);
            //call the angular http method
            this.http
        //post the form data to the url defined above and map the response. Then subscribe //to initiate the post. if you don't subscribe, angular wont post.
                .post(URL, formData).map((res:Response) => res.json()).subscribe(
                //map the success function and alert the response
                 (success) => {
                         //alert(success._body);
                         this.headerFieldData.next(success);
						 
                },
                (error) => alert(error))
          
  }
  
}
